---
layout: default
title: LibreArp
---

**LibreArp** is a free-form pattern arpeggiator.

If you encounter any bugs, have an enhancement idea, or any questions, **[please consider submitting an issue to the tracker]({{site.gitlab.repository_url}}/issues/)**.

If you find LibreArp useful, **[please consider sponsoring its author via PayPal](https://paypal.me/ostava)**.

{% youtube L3iqXnu_xs0 832 468 %}
