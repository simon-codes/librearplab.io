---
layout: page
permalink: /guide/homebrew/
title: Installing LibreArp via Homebrew
---

**Pre-requisite: [Homebrew must be installed](https://brew.sh)**

## Basic setup

In the terminal, run the following commands:

### Install

```
brew install librearp/librearp/librearp
update-librearp
```

### Update

```
brew update
update-librearp
```

### Uninstall

```
update-librearp remove
brew remove librearp
```

## update-librearp

The `update-librearp` script is able to create and remove links to the LibreArp binaries built using the Homebrew formula in the standard plugin search paths, either in the current user's home directory, or globally for the whole operating system.

**Command usage:** `update-librearp [action] [type]`

* **`action` - the action to take** (default: `install`)
    * `install` - (re-)creates the link
    * `remove` - deletes the link
* **`type` - installation type** (default: `local`)
    * `local` - for the current user
    * `global` - for all users

**Note:** Normally, when `type` is set to `global`, administrative privileges are required and the script needs to be run with `sudo`.

## Additional information

LibreArp's Homebrew formula is maintained in a tap [in its separate GitLab repository](https://gitlab.com/LibreArp/homebrew-librearp). This repository is [mirrored to GitHub](https://github.com/LibreArp/homebrew-librearp), so that `librearp/librearp` can be resolved by Homebrew.

By installing `librearp/librearp/librearp`, you install the `librearp` formula from the tap named `librearp` by the `librearp` organization. The formula downloads LibreArp's source code and builds it locally on your computer.

