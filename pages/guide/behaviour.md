---
layout: page
permalink: /guide/behaviour/
title: Behaviour settings
---

The *Behaviour* tab allows the user to set *per-instance* settings of the plugin. These settings will **not** be shared between individual instances of *LibreArp*.

![Behaviour settings](/assets/images/guide/behaviour/behaviour.png)

The **MIDI Input channel** parameter determines the channel that will be used as LibreArp's input. If it is set to *Any*, notes on all channels will be used. If set to a channel number, only notes from that channel will be used and notes on all other channels will be passed through.

The **MIDI Output channel** parameter determines the channel that will be used for notes that LibreArp generates.

When **Octave transposition** is enabled, notes that are outside of the *root octave* (see [Pattern editor](../pattern_editor)) will be transposed. Otherwise they will just wrap around.

When **Smart octaves** is enabled, LibreArp will calculate the number of octaves spanned by the current input - transposition will then be done by multiples of that number of octaves, ensuring that a higher LibreArp note will also always be audibly higher (analogically for lower notes).

When **Use input velocity** is enabled, the velocity of the input notes will be multiplied by the velocity of LibreArp notes. Otherwise only the LibreArp notes' velocities will be taken into account.

**Non-playing mode** determines the behaviour of the plugin when the host's playback is stopped:

* **Default (Global)** - Whatever is set in [Global settings](../global_settings)
* **Silence** - LibreArp plays nothing
* **Passthrough** - LibreArp passes all input notes through
* **Pattern** - LibreArp plays the drawn pattern

The **Chord size** parameter allows the user to set a fixed number of input notes. If there are fewer actual input notes, all excess LibreArp notes will be silent. If there are more actual input notes, LibreArp will pick the lowest or the highest notes, based on the **Note selection mode**.
