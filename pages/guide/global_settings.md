---
layout: page
permalink: /guide/global_settings/
title: Global settings
---

The *Global settings* tab can be used to change the global behaviour of all LibreArp instances.

![Global settings](/assets/images/guide/global_settings/global_settings.png)

**Check for updates automatically** periodically (once a day) checks the GitLab page for available updates to the plugin. This setting is opt-in.

**GUI scale** sets the scaling of the editor. May be useful for high-DPI screens.

When **Smooth scrolling** is enabled, the pattern editor will be smoothly animated when scrolled and/or zoomed. Otherwise the changes to scrolling and zooming will be instant.

**Global non-playing mode** sets the default value for *Non-playing mode*, as described on the [Behaviour tab](../behaviour).
