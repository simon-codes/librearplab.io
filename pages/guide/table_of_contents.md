---
layout: page
permalink: /guide/
title: LibreArp User Guide
---

Welcome to the user guide.

This guide is up-to-date with **LibreArp 2**. Please be aware that only the latest stable version (and the latest testing version, if newer than stable) of LibreArp is supported -- no official support will be provided to users of outdated versions.

This guide should give you a rough idea of how to use the LibreArp plugin. Should any questions arise or if you encounter any bugs while using the plugin, please consider submitting an issue to [the&nbsp;LibreArp issue tracker]({{site.gitlab.repository_url}}/issues/).

If you would like to add something to this guide, please consider submitting an issue or a merge request to [the LibreArp Page repository]({{site.gitlab.page_repository_url}}).

## Installation and DAW setup

* [Installation](install)
* [Set up LibreArp in your DAW](setup)

## Using the plugin

* [Basic principles](basic_principles)
* [Pattern editor](pattern_editor)
* [Behaviour settings](behaviour)
* [Global settings](global_settings)
