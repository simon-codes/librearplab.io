---
layout: page
permalink: /guide/setup/
title: Setting up LibreArp in your DAW
---

* [Ableton Live](live)
* [Ardour](ardour)
* [Bitwig Studio](bitwig)
* [FL Studio](fl)
* [Renoise](renoise)
* [Reaper](reaper)

If you know how to set up a DAW that is not listed above, or have an alternative way of setting up a listed DAW with LibreArp, please submit an issue or a merge request to [the LibreArp page repository]({{site.gitlab.page_repository_url}}).
