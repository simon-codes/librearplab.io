---
layout: page
permalink: /guide/basic_principles/
title: Basic principles of LibreArp
---

*LibreArp* is a MIDI effect plugin that takes chords in the form of MIDI input and outputs so-called *arpeggios* based on the input chords.

Arpeggios are generated using a user-created pattern (see [Pattern editor](/guide/pattern_editor/)). Each input note is given a number, based on its pitch (lowest to highest). The pattern then determines which note number should play when.

If a pattern contains an event for a note number higher than there is input notes, it wraps around and (by default) transposes the note by an octave. (e.g. the plugin gets a 3-note chord in its input but the pattern is set to play note no. 4 -- it actually plays note no. 1 transposed one octave higher than the input note).

The pattern has its loop length, i.e. its time loops around indefinitely in a song. The user can also set a loop-reset length, which resets the loop when a set amount of beats elapses.
