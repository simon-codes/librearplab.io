---
layout: page
permalink: /guide/install/
title: Installing LibreArp
---

## Windows

Simply extract the downloaded archive (`LibreArp.vst3.zip`) into your VST3 directory. This is usually `C:\Program Files\Common Files\VST3`, but it depends on your DAW of choice, so if it does not work, please check your DAW's settings and/or consult your DAW's manual.


## Linux

Simply extract the downloaded archive (`LibreArp.vst3.tar.gz` or `LibreArp.lv2.tar.gz`) into your plugin directory. For user-installed plugins, this is usually `/home/<username>/.vst3` or `/home/<username>/.lv2`, but it depends on your DAW of choice, so if it does not work, please check your DAW's settings and/or consult your DAW's manual.

Please note that the whole directory structure from the archive should be preserved in order for your DAW to correctly scan the plugin. So for VST3, there should be a hierarchy like this: `LibreArp.vst3/Contents/x86_64-linux`; for LV2, the plugin should be in a `LibreArp.lv2` directory.

*LibreArp VST3 for Linux may also be installed via [Homebrew](/guide/homebrew).*

## macOS

LibreArp VST3 for macOS may be [installed via Homebrew](/guide/homebrew).
