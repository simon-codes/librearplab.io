---
layout: page
title: The LibreArp contributors
permalink: /contributors/
---

## Direct contributors

* [**Oto Šťáva**](https://gitlab.com/Spiffyk)
    * Project lead
    * Programming
* [**Marek Jędrzejewski**](https://github.com/marekjedrzejewski) 
    * Look-and-feel design since version 2.0
    * Testing of the Homebrew macOS version

## Thanks to

* **Creators of the [JUCE framework](https://juce.com/)** - for the framework without which LibreArp wouldn't exist
* **[GitLab](https://gitlab.com) and [GitHub](https://github.com)** - for their great code sharing tools
* **Everyone who gave any sort of feedback** - because community provides the drive to carry on
* **All the DAW creators** - for giving artists the tools to express themselves
* **The awesome folks at various web forums** - for being awesome
